package g.CodinGame.GhostInTheCell;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Factory {
	int id;
	int owner;
	int production;
	int force;
	Map<Factory, Integer> linkedFactories;
	
	public Factory(int id){
		this.id = id;
		linkedFactories = new HashMap<>();
	}
	
	public void addLink(Factory f, int dist){
		linkedFactories.put(f, dist);
	}
	
	public List<Factory> canConquer(List<Factory> targets){
	    ArrayList<Factory> conquerables = new ArrayList<Factory>();
	    for(Factory f : targets){
	        if(!linkedFactories.containsKey(f)) continue;
	        if(this.force > f.force + ((f.owner == 0) ? 0: (f.production * this.linkedFactories.get(f))))
	            conquerables.add(f);
	    }
	    return conquerables;
	}
	
	public int priority(){
	    if(owner == -1) return production == 0 ? 1: 4;
	    else return production == 0 ? 0: 2;
	}
}
