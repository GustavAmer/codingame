package g.CodinGame.GhostInTheCell;

public class Troop {

	int id;
	int owner;
	Factory origin;
	Factory destination;
	int force;
	int turnsUntilArrival;
	
	public Troop(int id, int owner, Factory origin, Factory destination, int force, int turnsUntilArrival) {
		this.id = id;
		this.owner = owner;
		this.origin = origin;
		this.destination = destination;
		this.force = force;
		this.turnsUntilArrival = turnsUntilArrival;
	}
}
