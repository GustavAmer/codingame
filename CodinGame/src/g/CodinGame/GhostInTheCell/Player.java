package g.CodinGame.GhostInTheCell;

import java.util.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Player {

    public static void main(String args[]) {
        @SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
        int factoryCount = in.nextInt(); // the number of factories
        Factory[] factories = new Factory[factoryCount];
        List<Factory> mine;
        List<Factory> opponents;
        int bombsSent = 0;
        for(int i = 0; i < factoryCount; i++){
        	//factories[i] = new Player().new Factory(i); //CodinGame IDE supports only one file/main class
        	factories[i] = new Factory(i);
        }
        int linkCount = in.nextInt(); // the number of links between factories
        for (int i = 0; i < linkCount; i++) {
            int factory1 = in.nextInt();
            int factory2 = in.nextInt();
            int distance = in.nextInt();
            factories[factory1].addLink(factories[factory2], distance);
            factories[factory2].addLink(factories[factory1], distance);
        }

        List<Troop> troops;
        
    	int bombedFactory = -1;
        // game loop
        while (true) {
        	troops = new ArrayList<>();
        	mine = new ArrayList<>();
        	opponents = new ArrayList<>();
            int entityCount = in.nextInt(); // the number of entities (e.g. factories and troops)
            for (int i = 0; i < entityCount; i++) {
                int entityId = in.nextInt();
                String entityType = in.next();
                int arg1 = in.nextInt();
                int arg2 = in.nextInt();
                int arg3 = in.nextInt();
                int arg4 = in.nextInt();
                int arg5 = in.nextInt();
                if(entityType.equals("FACTORY")){
                	Factory f = factories[entityId];
                	f.owner = arg1;
                	f.force = arg2;
                	f.production = arg3;
                	if(arg1 == 1) mine.add(f);
                	else opponents.add(f);
                }else if(entityType.equals("TROOP")){
                	//troops.add(new Player().new Troop(entityId, arg1, factories[arg2], factories[arg3], arg4, arg5));
                	//CodinGame IDE supports only one file/main class
                	troops.add(new Troop(entityId, arg1, factories[arg2], factories[arg3], arg4, arg5));
                }
            }
           // System.err.println("my factories: " + mine.size());
           
           ArrayList<String> orders = new ArrayList<>();
           
           //awful looking bomb management
           if(bombsSent < 2){
               int minDist1 = Integer.MAX_VALUE;
               int bombSource1 = -1;
               int bombDest1 = -1;
               int minDist2 = Integer.MAX_VALUE;
               int bombSource2 = -1;
               int bombDest2 = -1;
               for(Factory f : opponents){
                   if(f.owner == 0 || f.id == bombedFactory) continue;
                   for(Factory oneOfMine: mine){
                       int d = oneOfMine.linkedFactories.get(f);
                       if(d < minDist1){
                           if(bombSource1 != bombSource2 && bombDest1 != bombDest2){
                               bombDest2 = bombDest1;
                               bombSource2 = bombSource1;
                               minDist2 = minDist1;
                           }
                           minDist1 = d;
                           bombSource1 = oneOfMine.id;
                           bombDest1 = f.id;
                       }else if(d < minDist2 && oneOfMine.id != bombSource1 && f.id != bombDest2){
                           minDist2 = d;
                           bombSource2 = oneOfMine.id;
                           bombDest2 = f.id;
                       }
                   }
               }
               if(bombSource1 != -1){
                   orders.add("BOMB " + bombSource1 + " " + bombDest1);
                   bombedFactory = bombDest1;
                   bombsSent++;
               }
               if(bombSource2 != -1){
                   orders.add("BOMB " + bombSource2 + " " + bombDest2);
                   bombsSent++;
               }
           }
           //End of shoddy bombing
           
           Map<Factory, List<Factory>> targets = new HashMap<Factory, List<Factory>>();
           for(Factory f: mine){
        	   List<Factory> conquerables = f.canConquer(opponents);
        	   if(!conquerables.isEmpty()){
        		   targets.put(f, conquerables);
        	   }
           }
           ArrayList<Factory> attacking = new ArrayList<>();
           for (int i = 4; i != 0; i--){
        	   for (Map.Entry<Factory, List<Factory>> entry : targets.entrySet()){
                    Factory source = entry.getKey();
                    for(Factory f: entry.getValue()){
                        if(!attacking.contains(f) && f.priority() == i){
                            int reqForceSize = f.force +1 + ((f.owner == 0)? 0:(f.production * source.linkedFactories.get(f)));
                            if(source.force < reqForceSize) continue;
                            orders.add("MOVE " + source.id + " " + f.id + " " + reqForceSize);
                            attacking.add(f);
                            source.force -= reqForceSize;
                        }
                    }
                }
            }
            for(Factory f: mine){
                if(f.force >= 10 && f.production < 3) orders.add("INC " + f.id);
            }
                
            if(orders.isEmpty()) System.out.println("WAIT");
            else{
                String printout = "";
                for(int i = 0; i < orders.size(); i++){
                    printout += orders.get(i);
                    if(i != orders.size()-1) printout+=";";
                }
                System.out.println(printout);  
            } 
            // Write an action using System.out.println()
            // To debug: System.err.println("Debug messages...");


            // Any valid action, such as "WAIT" or "MOVE source destination cyborgs"
            
        }
    }
}