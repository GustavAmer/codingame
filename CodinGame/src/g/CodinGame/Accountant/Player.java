package g.CodinGame.Accountant;

import java.util.ArrayList;
import java.util.Scanner;

public class Player {
	
	public static void main(String args[]) {
        Scanner in = new Scanner(System.in);

        ArrayList<Entity> datas = new ArrayList<Entity>();
        ArrayList<Entity> enemies = new ArrayList<Entity>();
        Entity player = new Entity(0,0,0);
        int L = 0;
        
        // game loop        
        boolean first = true;
        while (true) {
            datas.clear();
            enemies.clear();
            int x = in.nextInt();
            int y = in.nextInt();
            player.x = x;
            player.y = y;
            int dataCount = in.nextInt();
            for (int i = 0; i < dataCount; i++) {
                int dataId = in.nextInt();
                int dataX = in.nextInt();
                int dataY = in.nextInt();
                datas.add(new Entity(dataX, dataY, dataId));
            }
            int enemyCount = in.nextInt();
            for (int i = 0; i < enemyCount; i++) {
                int enemyId = in.nextInt();
                int enemyX = in.nextInt();
                int enemyY = in.nextInt();
                Entity e = new Entity(enemyX, enemyY, enemyId) ;
                e.life = in.nextInt();
                if(first) L += e.life;
                e.target = closest(e, datas);
                e.predict();
                enemies.add(e);
            }
            first = false;
            Entity clE = closest(player, enemies);
            Entity clD = closest(player, datas);
            
                        
            if(clE != null ){
                command(clE.id, "Shooting");
            }else if(clE != null){
                command(clE.x, clE.y, "testing");
            }else if(clD != null){
                command(clD.x, clD.y, "Hello");
            }else command(8000, 4500);
            
            // To debug: System.err.println("Debug messages...");
            // MOVE x y or SHOOT id
        }
    }
        
    public static boolean safe(Entity player, ArrayList<Entity> enemies){
        for(Entity e: enemies){
            if(d(player.nX, player.nY, e.nX, e.nY) <= 2000) return false;
        }return true;
    }
    
    public static int getDam(double dist){
        return (int)(Math.round(125000/Math.pow(dist, 1.2)));
    }
    
    public static double d(int x, int y, int x2, int y2){
        return Math.sqrt((x-x2)*(x-x2)+(y-y2)*(y-y2));
    }
    
    public static double d(Entity e1, Entity e2){
        return d(e1.x, e1.y, e2.x, e2.y);
    }
    
    public static double getAngle(Entity e1, Entity e2){
        return getAngle(e1.x, e1.y, e2.x, e2.y);
    }
    
    public static int getNextX(int x, double angle){
        return getNextX(x, angle, 1000);
    }
    
    public static int getNextY(int y, double angle){
        return getNextY(y, angle, 1000);
    }
    
    public static int getNextX(int x, double angle, int dist){
        return (int)(Math.floor(Math.cos(Math.toRadians(angle))*dist + x));
    }
    
    public static int getNextY(int y, double angle, int dist){
        return (int)(Math.floor(Math.sin(Math.toRadians(angle))*dist + y));
    }
    
    
    public static double getAngle(int x1, int y1, int x2, int y2){        
        double angle =  Math.toDegrees(Math.atan2(y2 - y1, x2 - x1));    
        if(angle < 0) angle += 360;
        return angle;
    }
    
    public static Entity closest(Entity point, ArrayList<Entity> targets){
        double min = Integer.MAX_VALUE;
        Entity closest  = null;
        double dist;
        for(Entity e: targets){
            dist = d(point, e);
            if(dist < min){
                min = dist;
                closest = e;
                e.tempDist = dist;
            }
        }
        return closest;
    }
    
    //Basic helper methods
    public static void command(int id, String... text){
        command("SHOOT " + id, text);
    }
    
    public static void command(int x, int y, String... text){
        command("MOVE " + x + " " + y, text);
    }
    
    public static void command(String baseCommand, String... text){
        String s = "";
        for(String t: text){
            s += t.toString() + " " ;
        }
        System.out.println(baseCommand + " " + s);
    }
    
    public static void print(Object... args){
        String s = "";
        for(Object t: args){
            s += t.toString() + " " ;
        }
        System.err.println(s);
    }
}
