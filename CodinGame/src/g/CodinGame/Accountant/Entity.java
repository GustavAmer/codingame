package g.CodinGame.Accountant;

public class Entity {
	int x;
    int y;
    int id;
    int life = 0;
    int nX;
    int nY;
    Entity target = null;
    double tempDist = Integer.MAX_VALUE;
    
    public Entity(int x, int y, int id){
        this.x = x;
        this.y = y;
        this.id = id;
    }    
    
    public void predict(){
        double eTargetAngle = Player.getAngle(this, this.target);
        nX = Player.getNextX(x, eTargetAngle);
        nY = Player.getNextY(y, eTargetAngle);
    }
}
